import router  from 'express';
import filters from './filters';

export default  [
  {
    method: 'GET',
    route: /^(\/admin)(?!\/api)(.*)$/,
    controller: {
      defaultAction: (req, res) => {
        return res.status(200).json(req.header);
      }
    },
    action: 'defaultAction'
  }
];
