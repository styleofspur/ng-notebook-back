import config from '../../../../config';
import fs     from 'fs';
import path   from 'path';

export default {

  /**
   *
   * @param req
   * @param res
   */
  defaultAction: (req, res) => {

    fs.readFile(path.resolve(`${config.paths.build}../front/admin/index.html`), {encoding: 'utf-8'}, (err,data) => {
      if (!err){
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
      } else {
        console.log(err);
      }

    });

  }

};