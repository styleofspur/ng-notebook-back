import config       from '../../../../config';
import CategoryRepo from '../repositories/CategoryRepo';

export default class CategoryController {

  /**
   *
   * @param req
   * @param res
   */
  static getTopParentsAction(req, res) {
    CategoryRepo.getTopParents().then(
        result => {
          res.status(200).json({
            status: 'success',
            data: {
              categories: result
            }
          });

        },
        () => {
          res.status(404).json({
            status: 'error',
            message: 'Not Found',
            data: {}
          });
        }
    );

  };

  /**
   *
   * @param req
   * @param res
   */
  static getChildrenAction(req, res) {
    CategoryRepo.getChildren(req.params.id).then(
        result => {
          res.status(200).json({
            status: 'success',
            data: {
              children: result
            }
          });

        },
        () => {
          res.status(404).json({
            status: 'error',
            message: 'Not Found',
            data: {}
          });
        }
    );

  };

  /**
   *
   * @param req
   * @param res
   */
  static getSiblingsAction(req, res) {
    CategoryRepo.getSiblings(req.params.id).then(
        result => {
          res.status(200).json({
            status: 'success',
            data: {
              siblings: result
            }
          });

        },
        () => {
          res.status(404).json({
            status: 'error',
            message: 'Not Found',
            data: {}
          });
        }
    );

  };

};