import config       from '../../../../config';
import UserRepo      from '../repositories/UserRepo';
import UserValidator from '../validators/UserValidator';

export default class AuthController {

  /**
   *
   * @param req
   * @param res
   */
  static loginAction(req, res) {

      UserRepo.find('creds', {
        username: req.body.username,
        password: req.body.password
      }).then(

        // success
        result => {
          res.status(200).json({
            status: 'success',
            data: {
              user: result
            }
          });
      },

      // validation or db error
      err => {
        res.status(404).json({
          status: 'error',
          message: err,
          data: {}
        });
      }

    );

  }

};