import config from '../../../../config';
import fs     from 'fs';
import path   from 'path';

export default class AnyController {

  /**
   *
   * @param req
   * @param res
   * @param next
   */
  static defaultAction(req, res, next) {

    fs.readFile(path.resolve(`${config.paths.frontend}/index.html`), {encoding: 'utf-8'}, (err, data) =>{
      if (!err){
        res.writeHead(200, {'Content-Type': 'text/html'});
        res.write(data);
        res.end();
      } else {
        console.log(err);
        next(err);
      }

    });

  }

};