import config       from '../../../../config';
import TagRepo      from '../repositories/TagRepo';
import TagValidator from '../validators/TagValidator';

export default class TagController {

  /**
   *
   * @param req
   * @param res
   */
  static getAllAction(req, res) {
    
    TagRepo.getAll().then(
      
        result => {
          res.status(200).json({
            status: 'success',
            data: {
              tags: result
            }
          });
      },
      
      () => {
        res.status(404).json({
          status: 'error',
          message: 'Not Found',
          data: {}
        });
      }
      
    );

  };

  /**
   *
   * @param req
   * @param res
   */
  static editAction(req, res) {

    TagValidator.validateEdit({
      name: req.params.name
    }).then(

        // validation success
        () => {

          TagRepo.edit(req.params.name, req.body.tag).then(
              result => {
                res.status(200).json({
                  status: 'success',
                  data: {
                    tag: result
                  }
                });

              },

              e => {
                res.status(500).json({
                  status: 'error',
                  message: e,
                  data: {}
                });
              }

          );

        },

        // validation error
        e => {
          return res.status(500).json({
            status: 'error',
            message: e,
            data: {}
          });
        }

    );

  };

  static createAction (req, res) {

    TagValidator.validateCreate(req.body).then(

        // validation success
        () => {

          TagRepo.create(req.body).then(

              result => {
                res.status(200).json({
                  status: 'success',
                  data: result
                });

              },

              e => {
                res.status(500).json({
                  status: 'error',
                  message: e,
                  data: {}
                });
              }

          );

        },

        // validation error
        e => {

          res.status(500).json({
            status: 'error',
            message: e,
            data: {}
          });

        }
    );

  };

  /**
   *
   * @param req
   * @param res
   */
  static deleteAction (req, res) {

    TagValidator.validateDelete({
      name: req.params.name
    }).then(

        // validation success
        () => {

          TagRepo.delete(req.params.name).then(

              result => {
                res.status(200).json({
                  status: 'success',
                  data: result
                });

              },

              e => {
                res.status(500).json({
                  status: 'error',
                  message: e,
                  data: {}
                });
              }

          );

        },

        // validation error
        e => {

          res.status(500).json({
            status: 'error',
            message: e,
            data: {}
          });

        }

    );

  };

};