import config           from '../../../../config';
import ArticleRepo      from '../repositories/ArticleRepo';
import ArticleValidator from '../validators/ArticleValidator';

export default class ArticleController {

  /**
   *
   * @param req
   * @param res
   */
  static getAllAction(req, res) {
    ArticleRepo.getAll().then(
      result => {
        res.status(200).json({
          status: 'success',
          data: {
            articles: result
          }
        });

      },
      () => {
        res.status(404).json({
          status: 'error',
          message: 'Not Found',
          data: {}
        });
      }
    );

  };

  /**
   *
   * @param req
   * @param res
   */
  static getFilteredAction(req, res) {
    ArticleRepo.find(req.params.by, req.params.cond).then(
      result => {
        res.status(200).json({
          status: 'success',
          data: {
            articles: result
          }
        });

      },
      () => {
        res.status(404).json({
          status: 'error',
          message: 'Not Found',
          data: {}
        });
      }
    );
  };

  /**
   *
   * @param req
   * @param res
   */
  static getByIdAction(req, res) {
    ArticleRepo.getById(req.params.id).then(
      result => {
        res.status(200).json({
          status: 'success',
          data: {
            article: result
          }
        });

      },
      () => {
        res.status(404).json({
          status: 'error',
          message: 'Not Found',
          data: {}
        });
      }
    );
  };

  /**
   *
   * @param req
   * @param res
   */
  static editAction(req, res) {

    ArticleValidator.validateEdit({
      id: req.params.id,
      article: req.body.article
    }).then(

      // validation success
        () => {
          ArticleRepo.edit(req.params.id, req.body.article).then(
            result => {
              res.status(200).json({
                status: 'success',
                data: {
                  article: result
                }
              });

            },
            e => {
              res.status(500).json({
                status: 'error',
                message: e,
                data: {}
              });
            }
          );
        },

      // validation error
        e => {
          return res.status(500).json({
            status: 'error',
            message: e,
            data: {}
          });
        }
      );

  };

  /**
   *
   * @param req
   * @param res
   */
  static createAction(req, res) {

    ArticleValidator.validateCreate(req.body.article).then(

        // validation success
        () => {
          ArticleRepo.create(req.body.article).then(
              result => {
                res.status(200).json({
                  status: 'success',
                  data: {
                    article: result
                  }
                });

              },
              e => {
                res.status(500).json({
                  status: 'error',
                  message: e,
                  data: {}
                });
              }
          );
        },

        // validation error
        e => {
          res.status(500).json({
            status: 'error',
            message: e,
            data: {}
          });
        }
    );

  };

  /**
   *
   * @param req
   * @param res
   */
  static deleteAction(req, res) {

    ArticleValidator.validateDelete({
      id: req.params.id
    }).then(

        // validation success
        () => {
          ArticleRepo.delete(req.params.id).then(
              result => {
                res.status(200).json({
                  status: 'success',
                  data: {
                    article: result
                  }
                });

              },
              e => {
                res.status(500).json({
                  status: 'error',
                  message: e,
                  data: {}
                });
              }
          );
        },

        // validation error
        e => {
          res.status(500).json({
            status: 'error',
            message: e,
            data: {}
          });
        }
    );

  };

};