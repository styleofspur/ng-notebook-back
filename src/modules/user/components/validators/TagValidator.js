import validate from 'validate.js';
  
import TagRepo  from '../repositories/TagRepo';

validate.validators.tagExists = (value, options, key, attributes) => {

  if (!value) return false;

  return new Promise((resolve, reject) => {

    TagRepo.getByName(value).then(resolve,
      err => {
        console.log(err);
        reject('Tag does not exist');
      }
    );

});

};

export default class TagValidator {

  static constraints = {

    edit: {

      'name': {
        presence:  true,
        tagExists: true,
        format:    {
          pattern: "[а-яА-Яa-zA-Z0-9\\(\\)\\[\\]\\'\'\\-\\_\\?\\!\\.\\,\\:\\/\\s]{1,1024}",
          flags:   'i'
        }
      }

    },

    create: {
      'name': {
        presence:      true,
        format:        {
          pattern: "[а-яА-Яa-zA-Z0-9\\(\\)\\[\\]\\'\'\\-\\_\\?\\!\\.\\,\\:\\/\\s]{1,1024}",
          flags:   'i'
        }
      }
    },

    delete: {
      name: {
        presence:  true,
        tagExists: true
      }
    }

  };
  
  validateEdit(data) {
    return validate.async(data, TagValidator.constraints.edit);
  };

  validateCreate(data) {
    return validate.async(data, TagValidator.constraints.create);
  };

  validateDelete(data) {
    return validate.async(data, TagValidator.constraints.delete);
  }

};