import validate from 'validate.js';
  
import UserRepo  from '../repositories/UserRepo';

validate.validators.login = (value, options, key, attributes) => {

  return new Promise((resolve, reject) => {

    UserRepo.find('creds', {
      username: attributes.username,
      password: value
    }).then(resolve,
      err => {
        console.log(err);
        reject('User does not exist');
      }
    );

});

};

export default class UserValidator {

  static constraints = {

    login: {

      'username': {
        presence:  true
      },
      password: {
        presence: true,
        login:    true
      }

    }

  };
  
  validateLogin(data) {
    return validate.async(data, UserValidator.constraints.login);
  };

};