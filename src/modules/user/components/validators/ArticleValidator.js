import validate    from 'validate.js';

import articleRepo from '../repositories/ArticleRepo';

// auth login validator
validate.validators.articleExists = (value, options, key, attributes) => {

  if (!value) return false;

  return new Promise( (resolve, reject) => {

    articleRepo.getById(value).then(resolve,
      err => {
        console.log(err);
        reject('Page does not exist');
      });

  });

};

export default class ArticleValidator {

  static constraints = {
    edit: {

      id: {
        presence:   true,
        articleExists: true
      },

      'article.title': {
        presence:      true,
        format:        {
          pattern: "[а-яА-Яa-zA-Z0-9\\(\\)\\[\\]\\'\"\\-\\_\\?\\!\\.\\,\\:\\/\\s]{1,1024}",
          flags:   'i'
        }
      }

    },

    create: {
      'title': {
        presence:      true,
        format:        {
          pattern: "[а-яА-Яa-zA-Z0-9\\(\\)\\[\\]\\'\"\\-\\_\\?\\!\\.\\,\\:\\/\\s]{1,1024}",
          flags:   'i'
        }
      }
    },

    delete: {
      id: {
        presence:   true,
        articleExists: true
      }
    }
  };

  validateEdit(data) {
    return validate.async(data, ArticleValidator.constraints.edit);
  };
  validateCreate(data) {
    return validate.async(data, ArticleValidator.constraints.create);
  };
  validateDelete(data) {
    return validate.async(data, ArticleValidator.constraints.delete);
  }
};