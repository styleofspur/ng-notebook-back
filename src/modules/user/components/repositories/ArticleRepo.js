import mongoose from 'mongoose';

import config   from '../../../../config';

let model         = mongoose.model('Article'),
    categoryModel = mongoose.model('Category');


export default class ArticleRepo {

  /**
   *
   */
  static getAll() {

    return new Promise((resolve, reject) => {
      model.find({ deletedAt: {
        $exists: false
      }}).populate({
        path: 'category',
        select: '_id name'
      }).exec( (err, result) => {
        if (err) {
          console.log(err);
          return resolve([]);
        }
        resolve(result);
      });
    });

  };

  /**
   *
   * @param by
   * @param cond
   * @returns {*}
   */
  static find(by, cond) {
    return ArticleRepo[`_findBy${by.charAt(0).toUpperCase() + by.slice(1)}`](cond);
  };

  /**
   *
   * @param tags
   * @returns {promise|*|promise|promise|Function|promise}
   * @private
   */
  static _findByTags(tags) {

    return new Promise( (resolve, reject) => {
      model.find({
        deletedAt: {
          $exists: false
        },
        tags: {
          $in: tags.split(',')
        }
      }).populate({
        path: 'category',
        select: '_id name'
      }).exec( (err, result) => {
        if (err) {
          console.log(err);
          return resolve([]);
        }
        resolve(result);
      });
    });

  };

  static parseCategoryParams(params) {
    let parts = params.split('/');

    // search by root category name
    if (parts.length  === 1) {
      return {name: parts[0]};
    } else {
      let path = params.substring(0,params.lastIndexOf('/'));
      let catName = params.substring(params.lastIndexOf('/')+1);
      return {
        path: path,
        name: catName
      }
    }

  };

  static _findByCategory(params) {

    return new Promise((resolve, reject) => {
      categoryModel.findOne(ArticleRepo.parseCategoryParams(params), (err) => {
        if (err) {
          return resolve([]);
        }
      }).then( (result) => {
        model.find({
          deletedAt: {
            $exists: false
          },
          category: result[0]._id
        }).populate({
          path: 'category',
          select: '_id name'
        }).exec( (err, result) => {
          if (err) {
            console.log(err);
            return resolve([]);
          }
          resolve(result);
        });
      });
    });

  };

  /**
   *
   * @param id
   * @returns {promise|*|promise|promise|Function|promise}
   */
  static getById(id) {

    return new Promise((resolve, reject) => {
      model.findById(id, (err, result) => {
        if (err) {
          return resolve([]);
        }
        resolve(result);
      });
    });

  };

  /**
   *
   * @param id
   * @param updateData
   * @returns {promise|*|promise|promise|Function|promise}
   */
  static edit(id, updateData) {

    return new Promise((resolve, reject) => {
      updateData.updatedAt = Date.now();
      model.findByIdAndUpdate(id, { $set: updateData}, (err, result) => {
        if (err) {
          console.log(err);
          return reject('DB Error');
        }
        resolve(result);
      });
    });

  };

  /**
   *
   * @param createData
   * @returns {promise|*|promise|promise|Function|promise}
   */
  static create(createData) {

    return new Promise((resolve, reject) => {
      model.create(createData, (err, result) => {
        if (err) {
          return reject('DB Error');
        }
        resolve(result);
      });
    });

  };

  /**
   *
   * @param id
   * @returns {promise|*|promise|promise|Function|promise}
   */
  static delete(id) {

    return new Promise((resolve, reject) => {
      model.findByIdAndUpdate(id, {deletedAt: Date.now()}, (err, result) => {
        if (err) {
          return reject('DB Error');
        }
        resolve(result);
      });
    });

  };

};