import mongoose from 'mongoose';

import config   from '../../../../config';

let model = mongoose.model('Category');

export default class CategoryRepo {

  /**
   *
   * @returns {*}
   */
  static getTopParents() {

    return new Promise( (resolve, reject) => {
      model.find( (err, categories) => {
        if (err || !categories) {
          return resolve([]);
        }

        let result = [];
        categories.forEach( category => {
          if (!category.parentId) {
            result.push({
              id:          category._id,
              name:        category.name,
              path:        category.path,
              hasChildren: false
            });
          }
        });

        result.forEach( (parent, i) => {

          categories.some( child => {
            if ( child.parentId && child.parentId.equals(parent.id) ) {
              result[i].hasChildren = true;
              return false;
            }

          })

        });

        resolve(result);

      });
    });

  };

  /**
   *
   * @param parentId
   * @returns {*}
   */
  static getChildren(parentId) {

    return new Promise((resolve, reject) => {
      model.find( (err, categories) => {
        if (err || !categories) {
          return resolve([]);
        }

        let result = [];

        categories.forEach( category => {

          if (category.parentId && category.parentId.equals(parentId)) {
            result.push({
              id:          category._id,
              name:        category.name,
              path:        category.path,
              hasChildren: false,
              parentId:    parentId
            });
          }

        });

        result.forEach( (child, i) => {

          categories.some( category => {

            if (category.parentId && category.parentId.equals(child.id)) {
              result[i].hasChildren = true;
              return false;
            }

          })

        });

        resolve(result);

      });
    });

  };

  /**
   *
   * @param siblingId
   * @returns {*}
   */
  static getSiblings(siblingId) {

    return new Promise((resolve, reject) => {
      model.findOne({ _id: siblingId }, (err, sibling) => {
        if (err || !sibling) {
          return resolve([]);
        }

        if (sibling.parentId) {
          CategoryRepo.getChildren(sibling.parentId).then( siblings => {
            resolve(siblings);
          });
        } else {
          CategoryRepo.getTopParents().then( siblings => {
            resolve(siblings);
          });
        }

      });
    });

  }

};