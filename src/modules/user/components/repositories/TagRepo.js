import mongoose     from 'mongoose';

import config       from '../../../../config';

let model        = mongoose.model('Tag'),
    articleModel = mongoose.model('Article');


export default class TagRepo {

  /**
   *
   */
  static getAll() {

    return new Promise((resolve, reject) => {

      model.find( (err, tags) => {
        if (err) {
          return resolve([]);
        }

        tags ? resolve(
          tags.map( tag => {
            return tag.name;
          })
        ) : resolve([]);

      });

    });

  };

  /**
   *
   * @param name
   * @returns {promise|*|promise|promise|Function|promise}
   */
  static getByName(name) {

    return new Promise((resolve, reject) => {
      model.findOne({ 'name': name }, (err, result) => {
        if (err) {
          return resolve([]);
        }
        resolve(result);
      });
    });

  };


  static edit(name, updateData) {

    return new Promise((resolve, reject) => {
      model.update({ name: name }, updateData).exec()
        .then(() => {

          return articleModel.update({
            tags: name
          }, {
            $push: {
              tags: updateData.name
            }
          }).exec();

        })
        .then( () => {

          return articleModel.update({
            tags: name
          }, {
            $pull: {
              tags: name
            }
          }).exec();

        })
        .then( () => {
          resolve(updateData);
        });
    });

  };

  static create(createData) {

    return new Promise((resolve, reject) => {
      model.create(createData, (err, result) => {
        if (err) {
          return reject('DB Error');
        }
        resolve(result);
      });
    });

  };

  /**
   *
   * @param name
   * @returns {promise|*|promise|promise|Function|promise}
   */
  static delete(name) {

    return new Promise((resolve, reject) => {
      model.remove({ name: name }).exec()
        .then( () => {

          return articleModel.update({
            tags: name
          }, {
            $pull: {
              tags: name
            }
          }).exec();

        })
        .then(resolve);
    });

  }

};