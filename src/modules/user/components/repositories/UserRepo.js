import mongoose from 'mongoose';
import faker    from 'faker';

import config   from '../../../../config';

let model = mongoose.model('User');

export default class UserRepo {

  /**
   *
   * @param by
   * @param cond
   * @returns {*}
   */
  static find(by, cond) {
    return UserRepo[`_findBy${by.charAt(0).toUpperCase() + by.slice(1)}`](cond);
  };

  /**
   *
   * @param cond
   * @returns {Promise}
   * @private
   */
  static _findByToken(cond) {

    return new Promise( (resolve, reject) => {
      model.find(cond).exec( (err, result) => {
        if (err) {
          console.log(err);
          return reject(err);
        }
        resolve(result);
      });
    });

  };

  /**
   *
   * @param creds
   * @returns {Promise}
   * @private
   */
  static _findByCreds(creds) {

    return new Promise( (resolve, reject) => {

      if (!creds.username || !creds.password) {
        reject('Invalid credentials');
      }

      model.findOne({username: creds.username}).exec( (err, user) => {

        if (err) {
          return reject(err);
        }

        if (!user || creds.password !== user.password) {
          return reject('Invalid credentials');
        }

        user.authToken    = faker.internet.password();
        user.lastAuthDate = Date.now();
        user.save(err => {
          if (err) return reject(err);
          return resolve({
            id:        user._id,
            username:  user.username,
            authToken: user.authToken
          });
        });

      });

    });

  };


};