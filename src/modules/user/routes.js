import { Router }   from 'express';

import filter       from './filters';
import AnyCtrl      from './components/controllers/AnyController';
import AuthCtrl      from './components/controllers/AuthController';
import TagCtrl      from './components/controllers/TagController';
import CategoryCtrl from './components/controllers/CategoryController';
import ArticleCtrl  from './components/controllers/ArticleController';

let router = new Router();
let routes = [
  {
    method:     'GET',
    route:      /^(\/)(?!api)(?!admin)(.*)$/,
    controller: AnyCtrl,
    action:     'defaultAction'
  },
  {
    method:     'POST',
    route:      '/api/login',
    controller: AuthCtrl,
    action:     'loginAction'
  },
  {
    method:     'GET',
    route:      '/api/tags',
    controller:  TagCtrl,
    action:     'getAllAction'
  },
  {
    method:     'PUT',
    route:      '/api/tag/:name',
    controller:  TagCtrl,
    action:     'editAction'
  },
  {
    method:     'POST',
    route:      '/api/tag/create',
    controller:  TagCtrl,
    action:     'createAction'
  },
  {
    method:     'DELETE',
    route:      '/api/tag/:name',
    controller:  TagCtrl,
    action:     'deleteAction'
  },
  {
    method:     'GET',
    route:      '/api/categories/top-parents',
    controller:  CategoryCtrl,
    action:     'getTopParentsAction'
  },
  {
    method:     'GET',
    route:      '/api/categories/:id/children',
    controller:  CategoryCtrl,
    action:     'getChildrenAction'
  },
  {
    method:     'GET',
    route:      '/api/categories/:id/siblings',
    controller:  CategoryCtrl,
    action:     'getSiblingsAction'
  },
  {
    method:     'GET',
    route:      '/api/articles',
    controller:  ArticleCtrl,
    action:     'getAllAction'
  },
  {
    method:     'GET',
    route:      '/api/articles/:by/:cond*',
    controller:  ArticleCtrl,
    action:     'getFilteredAction'
  },
  {
    method:     'GET',
    route:      '/api/article/:id',
    controller:  ArticleCtrl,
    action:     'getByIdAction'
  },
  {
    method:     'PUT',
    route:      '/api/article/:id',
    controller:  ArticleCtrl,
    action:     'editAction'
  },
  {
    method:     'POST',
    route:      '/api/article/create',
    controller:  ArticleCtrl,
    action:     'createAction'
  },
  {
    method:     'DELETE',
    route:      '/api/article/:id',
    controller:  ArticleCtrl,
    action:     'deleteAction'
  }
];

export default routes;
