import mongoose from 'mongoose';

import UserRepo from './components/repositories/UserRepo';

/**
 *
 */
export default {

  /**
   *
   * @param req
   * @param res
   * @param next
   * @returns {*}
   */
  checkToken: (req, res, next) => {

    let errorStatus = 401;
    let errorResp = {
      status: 'error',
      message: 'Not authorized',
      data: {}
    };
    let token = req.get('X-Auth-Token');

    if(!token) {
      res.status(errorStatus).json(errorResp);
      return next(errorResp);
    }

    UserRepo.find('token', { authToken: token });


  }


}