import localConfig from '../config.json';

// Set the current environment to true in the env object
let currentEnv  = process.env.NODE_ENV || 'development';

let config = {

  appName: "NgNotebook",

  env:      {
    production:  false,
    staging:     false,
    test:        false,
    development: false
  },

  log:      {
    path: `${__dirname}/var/log/app_#{currentEnv}.log`
  },

  db: {
    url:      localConfig.db.url,
    database: localConfig.db.database,
    user:     localConfig.db.user,
    pwd:      localConfig.db.pwd,
    entities: [
      'article', 'category', 'setting', 'tag', 'user'
    ],
    collections: [
      'articles', 'categories', 'settings', 'tags', 'users'
    ]
  },

  server:   {
    port: 9600,
    // In staging and production, listen loopback. nginx listens on the network.
    ip: '127.0.0.1'
  },

  paths: {
    src:                `${__dirname}/src`,
    frontend:           localConfig.paths.frontend,
    build:              `${__dirname}/`,
    loader:             `${__dirname}/loader`,
    modules:            `${__dirname}/modules`,
    entities:           `${__dirname}/database/entities`,
    moduleControllers:  '/controllers',
    migrations:         `${__dirname}/database`,
    seeds:              `${__dirname}/database`,
    moduleRoutes:       '/routes.js'
  },

  modules: {
    admin: {
      components: {
        dirs:  [
          'components'
        ],
        files: [
          'filters.js',
          'routes.js'
        ]
      }
    },
    user: {
      components: {
        dirs:  [
          'components'
        ],
        files: [
          'filters.js',
          'routes.js'
        ]
      }
    }
  }

};

config.env[currentEnv] = true;

if (currentEnv != 'production' && currentEnv != 'staging') {
  config.enableTests = true;
  // Listen on all IPs in dev/test (for testing from other machines)
  config.server.ip = '0.0.0.0';
}

export default config;