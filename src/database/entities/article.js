import { Schema } from 'mongoose';

export default {
  name: 'Article',
  desc: {
    title: String,
    content: String,
    category: {
      type:     Schema.Types.ObjectId,
      ref:      'Category',
      required: false
    },
    tags: Array,
    createdAt: {
      type:    Date,
      default: Date.now
    },
    updatedAt: {
      type:    Date
    },
    deletedAt: {
      type:    Date
    }
  }
};