export default {
  name: 'User',
  desc: {
    username: String,
    password: String,
    authToken: String,
    lastAuthDate: {
      type: Date,
      default: Date.now
    }
  }
};