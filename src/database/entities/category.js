import { Schema } from 'mongoose';

module.exports = {
  name: "Category",
  desc: {
    name:     String,
    parentId: {
      type:     Schema.Types.ObjectId,
      ref:      'Category',
      required: false
    },
    path: {
      type:     String,
      required: false
    }
  }
};