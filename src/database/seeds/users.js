var config   = require('../../config'),
    faker    = require('faker'),
    bcrypt   = require('bcrypt'),
    User     = require('mongoose').model("User"),
    salt     = bcrypt.genSaltSync(10);


function run(next) {
  if (process.argv.indexOf('--clean') === -1) {
    return seed(next);
  } else {
    return clean(next);
  }
}


/**
 *
 */
function seed(next) {

  var user = new User({
        username:     'Test User',
        password:     bcrypt.hashSync('password', salt),
        authToken:    faker.internet.password(),
        lastAuthDate: Date.now()
      });

  user.save(function(err) {
    if (err) {
      console.log('Users collection seeding error: ', err.message);
      throw err;
    } else {
      console.log('Users collection seeded');
      next();
    }

  });

}

/**
 *
 */
function clean(next) {

  User.remove({}, function(err) {
    if (err) {
      console.log('Users collection removing error: ', err.message);
      throw err;
    } else {
      console.log('Users collection dropped');
      next();
    }

  });

}

module.exports.run = run;