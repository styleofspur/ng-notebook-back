var config     = require('../../config'),
    faker      = require('faker'),
    mongoose   = require("mongoose"),
    Article    = mongoose.model("Article"),
    Category   = mongoose.model('Category'),
    _          = require('lodash'),
    categories = [];

function run(next) {
  if (process.argv.indexOf('--clean') === -1) {
    Category.find(function(err, data) {
      if (err) {
        console.log('Error while seeding Articles collection.');
        throw err;
      }
      categories = data;
      return seed(next);
    });
  } else {
    return clean(next);
  }
}

/**
 *
 */
function seed(next) {

  var saved    = 0,
      articles = [
        new Article({
          title: 'Try AngularJS Directives',
          content: faker.lorem.paragraphs(),
          category: _.find(categories, {name: 'Programming'})._id,
          tags: ['angularjs-directives']
        }),
        new Article({
          title: 'Introduction to Artisan',
          content: faker.lorem.paragraphs(),
          category: _.find(categories, {name: 'Programming'})._id,
          tags: ['laravel', 'artisan']
        })
      ];

  articles.forEach(function(article) {
    article.save(function(err) {

      if (err) {
        console.log('Articles collection seeding error: ', err.message);
        throw err;
      } else {
        saved++;
        if (saved === articles.length) {
          console.log('Articles collection seeded');
          next();
        }
      }

    });

  });

}

/**
 *
 */
function clean(next) {

  Article.remove({}, function(err) {
    if (err) {
      console.log('Articles collection removing error: ', err.message);
      throw err;
    } else {
      console.log('Articles collection dropped');
      next();
    }

  });

}

module.exports.run = run;