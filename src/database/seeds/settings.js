var config   = require('../../config'),
    mongoose = require("mongoose"),
    Setting  = mongoose.model("Setting"),
    q        = require('q');

function run(next) {
  if (process.argv.indexOf('--clean') === -1) {
    return seed(next);
  } else {
    return clean(next);
  }
}

/**
 *
 */
function seed(next) {
  var setting = new Setting({
        widgets: {
          calendar: {
            enabled: true
          }
        },
        site: {
          title:      "My Ng-Notebook",
          copyrights: "Ng-Notebook Team"
        }
      });

  setting.save(function(err) {

    if (err) {
      console.log('Settings collection seeding error: ', err.message);
      throw err;
    } else {
      console.log('Settings collection seeded');
      next();
    }

  });

}

/**
 *
 */
function clean(next) {

  Setting.remove({}, function(err) {
    if (err) {
      console.log('Settings collection removing error: ', err.message);
      throw err;
    } else {
      console.log('Settings collection dropped');
      next();
    }

  });

}

module.exports.run = run;