var config         = require('../../config'),
    migrationsPath = config.paths.migrations + '/migrations',
    database       = require('../../database'),
    mongoose       = require("mongoose"),
    fs             = require('fs'),
    dm             = require('node-dm'),
    q              = require('q'),
    _              = require('lodash'),
    async          = require('async');

function runSeeders() {

  fs.readdir(migrationsPath, function (err, files) {
    if (err) {
      console.log('Error while reading migrations directory "' + migrationsPath + '": ', err.message);
      throw err;
    }

    var seeders = [];

    _.map(files, function(file) {
      if (file.match(/^\d/)) {
        var entity = file.split('.')[0].split('-')[1];
        if (config.db.collections.indexOf(entity) !== -1) seeders.push(require('./' + entity).run);
      }

    });

    async.waterfall(seeders.concat(function() {

      var operation = process.argv.indexOf('--clean') === -1 ? 'seeded' : 'cleaned';

      console.log('DB successfully ' + operation +'!');
      process.exit(0);
    }));

  });
}

database.run(runSeeders);