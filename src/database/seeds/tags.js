var config   = require('../../config'),
    mongoose = require('mongoose'),
    Tag      = mongoose.model("Tag");

function run(next) {
  if (process.argv.indexOf('--clean') === -1) {
    return seed(next);
  } else {
    return clean(next);
  }
}

/**
 *
 */
function seed(next) {

  var saved  = 0,
      tags   = [
        new Tag({
          name: 'angularjs-directives'
        }),
        new Tag({
          name: 'angularjs-services'
        }),
        new Tag({
          name: 'nodejs'
        }),
        new Tag({
          name: 'npm'
        }),
        new Tag({
          name: 'laravel'
        }),
        new Tag({
          name: 'artisan'
        }),
      ];

  tags.forEach(function(tag) {
    tag.save(function(err) {

      if (err) {
        console.log('Tags collection seeding error: ', err.message);
        throw err;
      } else {
        saved++;
        if (saved === tags.length) {
          console.log('Tags collection seeded');
          next();
        }
      }

    });
  });

}

/**
 *
 */
function clean() {

  Tag.remove({}, function(err) {
    if (err) {
      console.log('Tags collection removing error: ', err.message);
      throw err;
    } else {
      console.log('Tags collection dropped');
      next();
    }

  });

}

module.exports.run = run;