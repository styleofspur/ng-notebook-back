var config     = require('../../config'),
    mongoose   = require("mongoose"),
    Category   = mongoose.model("Category"),
    categories = [
      {
        name:     'Philosophy',
        children: ['Spinoza', 'Deleuze', 'Nietzsche']
      },
      {
        name: 'Programming',
        children: ['JavaScript', 'PHP']
      }
    ];


function run(next) {
  if (process.argv.indexOf('--clean') === -1) {
    return seed(next);
  } else {
    return clean(next);
  }
}

/**
 *
 */
function seed(next) {

  var saved  = 0;

  categories.forEach(function(item) {

    var parentCategory = new Category({
      name: item.name
    });

    parentCategory.save(function(err, savedParentCategory) {
      if (err) {
        console.log('Categories collection seeding error: ', err.message);
        throw err;
      } else {
        saved++;

        item.children.forEach(function(child, i) {

          var childCategory = new Category({
            name:     child,
            parentId: savedParentCategory._id
          });

          childCategory.save(function(err) {

            if (err) {
              console.log('Categories collection seeding error: ', err.message);
              throw err;
            } else if ( (i + 1) === item.children.length  && saved === categories.length ) {
              console.log('Categories collection seeded');
              next();
            }

          });

        });

      }


    });
  });

}

/**
 *
 */
function clean(next) {

  var result = q.defer();

  Category.remove({}, function(err) {
    if (err) {
      console.log('Categories collection removing error: ', err.message);
      throw err;
    } else {
      next();
      console.log('Categories collection dropped');
    }

  });

}

module.exports.run = run;