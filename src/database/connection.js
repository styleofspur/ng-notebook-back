import faker    from 'faker';
import mongoose from 'mongoose';

import config   from '../config';

export default {
  run: (cb) => {

    console.log('DB connection started...');

    mongoose.connect(config.db.url || 'mongodb://127.0.0.1:27017/ng-notebook', {
      user: config.db.user,
      pass: config.db.pwd
    }, err => {

      if(err){
        throw (err);
      }
      else{
        console.log('Connected to DB with mongoose.', config.db.url);
        loadModels(cb);
      }

    });

    function loadModels(cb) {
      let entitiesPath = config.paths.entities,
          entitiesList = config.db.entities,
          models       = {};

      entitiesList.forEach( (entity) => {

        let entityData = require(`${entitiesPath}/${entity}`);
        let schema     = new mongoose.Schema(entityData.desc);
        mongoose.model(entityData.name, schema);
        models[entity] = mongoose.model(entityData.name);

      } );

      console.log('DB models initialized.');
      cb();

    }


  }
};