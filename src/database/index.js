import config     from '../config';
import connection from './connection';

export default {

  run: (cb) => {
    connection.run(cb);
  }

};
