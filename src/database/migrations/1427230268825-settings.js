var mongoose    = require("mongoose"),
    db          = require('./db'),
    config      = require('../../config');

exports.up = function(next){

  db.call(function(next) {
    db.create('settings', next);
  }, next);

};

exports.down = function(next){

  db.call(function(next) {
    db.drop('settings', next);
  }, next);

};
