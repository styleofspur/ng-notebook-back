var mongoose    = require("mongoose"),
    db          = require('./db'),
    config      = require('../../config');

exports.up = function(next){

  db.call(function(next) {
    upFn(next);
  }, next);

};

exports.down = function(next){

  db.call(function(next) {
    downFn(next);
  }, next);

};


function upFn(next) {

  mongoose.connection.db.collection('articles').update({}, {
    $set: {
      createdAt: Date.now()
    }
  }, {
    multi: true
  }, next);

}

function downFn(next) {
  mongoose.connection.db.collection('articles').update({}, {
    $unset: {
      createdAt: ""
    }
  }, {
    multi: true
  }, next);
}