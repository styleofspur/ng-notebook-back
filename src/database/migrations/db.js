var mongoose = require("mongoose"),
    config = require('../../config');

var conn = mongoose.connect(config.db.url || "mongodb://127.0.0.1:27017/ng-notebook", {
  user: config.db.user,
  pass: config.db.pwd
});

module.exports = {

  create: function(collection, next) {

    mongoose.connection.db.createCollection(collection, function(err, res) {

      if (err) {
        console.log('Creating collection error: ', err.message);
        throw err;
      }

      next();
    });

  },

  drop: function(collection, next) {

    mongoose.connection.db.dropCollection(collection, function(err, res) {

      if (err) {
        console.log('Dropping collection error: ', err.message);
        throw err;
      }

      next();
    });

  },

  call: function(cb, next) {

    if (mongoose.connection.readyState === 1) {

      cb(next);

    } else {
      mongoose.connection.on('connected', function(err) {

        if (err) {
          console.log('DB connection error');
          throw err;
        }

        cb(next);

      });
    }

  }

};
