var mongoose     = require("mongoose"),
    db           = require('./db'),
    config       = require('../../config'),
    entitiesPath = config.paths.entities,
    entitiesList = config.db.entities;

entitiesList.forEach(function (entity) {

  var entityData = require(entitiesPath + '/' + entity);
  var schema     = new mongoose.Schema(entityData.desc);
  mongoose.model(entityData.name, schema);

});

var Article  = mongoose.model('Article'),
    Category = mongoose.model('Category');

exports.up = function(next){


  db.call(function(next) {
    upFn(next);
  }, next);

};

exports.down = function(next){

  db.call(function(next) {
    downFn(next);
  }, next);

};


function upFn(next) {

  Category.find(function(err, categories) {

    var topParents = [];
    categories.forEach(function(category) {
      if (!category.parentId) topParents.push(category);
    });


    function combinePaths(parents, all) {

      var p = [];

      all.forEach(function(item, i) {

        parents.forEach(function(parent) {

          if (item.parentId && item.parentId.equals(parent._id)) {
            all[i].path = parent.path ? parent.path + '/' + parent.name : parent.name;
            p.push(item);
            all[i].save();
          }

        });


      });

      if (p.length) {
        combinePaths(p, all)
      } else next();

    }

    combinePaths(topParents, categories);

  });

}

function downFn(next) {
  mongoose.connection.db.collection('categories').update({}, {
    $unset: {
      path: ""
    }
  }, {
    multi: true
  }, next);
}
