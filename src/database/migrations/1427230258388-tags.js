var mongoose    = require("mongoose"),
    db          = require('./db'),
    config      = require('../../config');

exports.up = function(next){

  db.call(function(next) {
    db.create('tags', next);
  }, next);

};

exports.down = function(next){

  db.call(function(next) {
    db.drop('tags', next);
  }, next);

};
