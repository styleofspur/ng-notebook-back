var mongoose    = require("mongoose"),
    db          = require('./db'),
    config      = require('../../config');

exports.up = function(next){

  db.call(function(next) {
    db.create('categories', next);
  }, next);

};

exports.down = function(next){

  db.call(function(next) {
    db.drop('categories', next);
  }, next);

};
