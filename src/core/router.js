import config                    from '../config';
import {Router as ExpressRouter} from 'express';

/**
 *
 */
export default class Router {

  static _router = ExpressRouter();

  /**
   *
   * @param {object} app
   * @param {array} routes
   */
  static init (app, routes) {

    if (routes.prefix) {
      app.use(`/${routes.prefix}`, Router._setupPrefixedRoutes(routes));
    } else {
      Router._setupCommonRoutes(...arguments);
    }

  };

  /**
   *
   * @param app
   * @param {array} routes
   * @private
   */
  static _setupCommonRoutes (app, routes) {

    routes.forEach( route => {
          let args    = [],
              method  = route.method.toLowerCase(),
              path    = route.route,
              handler = route.controller ?
                        (req, res, next) => {
                          route.controller[route.action](req, res, next);
                        }
                        :
                        route.handler;

          args.push(path);
          if (route.access) {
            args.push(route.access)
          }
          args.push(handler);

          app[method].apply(app, args);
        }
    );

  };

  /**
   *
   * @param {array} routes
   * @protected
   */
  static _setupPrefixedRoutes(routes) {
    routes.forEach( route => {
      Router._setupRoute(Router._router, route)
    });

    return Router._router;
  };

  /**
   *
   * @param router
   * @param {*} settings
   * @protected
   */
  static _setupRoute(router, settings) {

    var params         = [settings.route],
        requestHandler = settings.handler || settings.action;

    if (settings.access) {
      params.push(settings.access);
    }

    // handler: function() {...} or handler: [fn() {...}, fn() {...} ... ]
    if (requestHandler instanceof Array) {
      requestHandler.forEach( f => {
        params.push(Router._getHandler(settings.controller || null, f));
      });
    }
    else {
      params.push(Router._getHandler(settings.controller || null, requestHandler));
    }

    switch (settings.method) {
      case 'ALL':
        router.all.apply(router, params);
        break;
      case 'USE':
        router.use.apply(router, params);
        break;
      case 'GET':
        router.get.apply(router, params);
        break;
      case 'POST':
        router.post.apply(router, params);
        break;
      case 'PUT':
        router.put.apply(router, params);
        break;
      case 'DELETE':
        router.delete.apply(router, params);
        break;
      case 'PARAM':
        router.param.apply(router, params);
    }

  };

  /**
   *
   * @param {object|null} controller
   * @param {function} handler
   * @returns {Function}
   * @protected
   */
  static _getHandler(controller, handler) {
    return controller ?
        (req, res, next) => {
          controller[handler](req, res, next);
        } : handler;
  };

};