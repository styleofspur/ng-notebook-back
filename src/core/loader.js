/* global require */
import path   from 'path';
import fs     from 'fs';

import config from '../config';
import Router from './router';

/**
 *
 */
 export default class Loader {

  static _paths   = config.paths;
  static _modules = config.modules;
  static _app     = null;

  /**
   *
   */
  static init(app) {

    Loader._app = app;

    for(var moduleName in Loader._modules) {

      var components = [];

      if (Loader._modules[moduleName].components) {
        Object.keys(Loader._modules[moduleName].components).forEach( key => {
          components = components.concat(Loader._modules[moduleName].components[key]);
        });
      }

      console.log(`'Loading <<${moduleName}>> module files...`);
      Loader._loadModuleFiles(`${Loader._paths.modules}/${moduleName}`, components);
      console.log(`<<${moduleName}>> module files are successfully loaded`);

    }

  };

  /**
   *
   * @param path
   * @param components
   * @private
   */
  static _loadModuleFiles(path, components) {

    fs.readdirSync(path).forEach( item => {

      var doLoading = (components && components.length) ? components.indexOf(item) !== -1 : true;

      if (doLoading) {

        if (item.substr(-3) === '.js') { // not file - call the function
          if (item === 'routes.js') {
            Router.init(Loader._app, require(`${path}/${item}`));
          } else {
            require(`${path}/${item}`);
          }
        } else {
          Loader._loadModuleFiles(`${path}/${item}`);
        }

      }

    });

  };

};