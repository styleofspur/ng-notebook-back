import express      from 'express';
import path         from 'path';
import cookieParser from 'cookie-parser';
import cors         from 'cors';
import bodyParser   from 'body-parser';

import config       from './config';
import Loader       from './core/loader';
import database     from './database';

let  app          = express(),
     db           = database.run(() => {
      // loading application modules
       Loader.init(app);
      addErrorHandlers();
    });

app.use(cors());

//app.use(logger('dev'));
app.use(bodyParser.json({
  limit: '100mb'
}));
app.use(bodyParser.urlencoded({
  extended: false,
  limit:    '100mb'
}));
app.use(cookieParser());
app.use('/frontend', express.static(path.resolve(config.paths.frontend)));

function addErrorHandlers() {
  // catch 404 and forward to error handler
  app.use( (req, res, next) => {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

// error handlers

// development error handler
// will print stacktrace
  if (app.get('env') === 'development') {
    app.use( (err, req, res, next) => {
      res.status(err.status || 500);
      res.json({
        message: err.message,
        error:   err
      });
    });
  }

// production error handler
// no stacktraces leaked to user
  app.use( (err, req, res, next) => {
    res.status(err.status || 500);
    res.json({
      message: err.message,
      error:   {}
    });
  });
}

export default app;